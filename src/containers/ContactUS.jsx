import { motion, useAnimation } from "framer-motion";
import { useInView } from "react-intersection-observer";
import { useEffect } from "react";
import location from '../imgs/Contact/icons8-location-48.png'
import call from '../imgs/Contact/icons8-call-50.png'
import message from '../imgs/Contact/icons8-message-64.png'

const ContactAnime = (e) => ({
    visible: { opacity: 1, scale: 1, y: 0, transition: { duration: e } },
    hidden: { opacity: 1, scale: 1, y: 100 },
});

export default function ContactUS() {
    const Control = useAnimation()
    const [ref, inView] = useInView()

    useEffect(() => {
        if (inView) {
            Control.start("visible")
        }
    }, [inView])



    return (
        <motion.section
            ref={ref}
            variants={ContactAnime(1)}
            initial="hidden"
            animate={Control}
        >
            <div className="section-title text-center my-10">
                <h2 className="text-3xl uppercase font-bold my-2 max-[500px]:text-lg">Contact US</h2>
            </div>
            <div className="grid grid-cols-3 gap-2 place-content-center mx-32 my-10
                            max-[1100px]:grid-cols-2 max-md:grid-cols-1 max-md:mx-10">
                <div className="flex flex-col gap-4">
                    <h1 className="text-2xl font-bold">Vesperr</h1>
                    <p>
                        Cras fermentum odio eu feugiat. Justo eget magna fermentum iaculis eu non diam phasellus. Scelerisque felis imperdiet proin fermentum leo. Amet volutpat consequat mauris nunc congue.
                    </p>
                    <div className="flex items-center gap-2">
                        <a href="">
                            <svg className="p-1 rounded-full border border-blue-500 fill-blue-500 transition-all duration-500 ease-in-out
                                          hover:bg-blue-500 hover:fill-white" viewBox="0 0 24 24" width="36px" height="36px">
                                <path d="M21.634,4.031c-0.815,0.385-2.202,1.107-2.899,1.245c-0.027,0.007-0.049,0.016-0.075,0.023 c-0.813-0.802-1.927-1.299-3.16-1.299c-2.485,0-4.5,2.015-4.5,4.5c0,0.131-0.011,0.372,0,0.5c-3.218,0-5.568-1.679-7.327-3.837 C3.438,4.873,3.188,5.024,3.136,5.23C3.019,5.696,2.979,6.475,2.979,7.031c0,1.401,1.095,2.777,2.8,3.63 c-0.314,0.081-0.66,0.139-1.02,0.139c-0.424,0-0.912-0.111-1.339-0.335c-0.158-0.083-0.499-0.06-0.398,0.344 c0.405,1.619,2.253,2.756,3.904,3.087c-0.375,0.221-1.175,0.176-1.543,0.176c-0.136,0-0.609-0.032-0.915-0.07 c-0.279-0.034-0.708,0.038-0.349,0.582c0.771,1.167,2.515,1.9,4.016,1.928c-1.382,1.084-3.642,1.48-5.807,1.48 c-0.438-0.01-0.416,0.489-0.063,0.674C3.862,19.504,6.478,20,8.347,20C15.777,20,20,14.337,20,8.999 c0-0.086-0.002-0.266-0.005-0.447C19.995,8.534,20,8.517,20,8.499c0-0.027-0.008-0.053-0.008-0.08 c-0.003-0.136-0.006-0.263-0.009-0.329c0.589-0.425,1.491-1.163,1.947-1.728c0.155-0.192,0.03-0.425-0.181-0.352 c-0.543,0.189-1.482,0.555-2.07,0.625c1.177-0.779,1.759-1.457,2.259-2.21C22.109,4.168,21.895,3.907,21.634,4.031z" />
                            </svg>
                        </a>
                        <a href="">
                            <svg className="p-1 rounded-full border border-blue-500 fill-blue-500 transition-all duration-500 ease-in-out
                                          hover:bg-blue-500 hover:fill-white" viewBox="0 0 30 30" width="36px" height="36px">
                                <path d="M15,3C8.373,3,3,8.373,3,15c0,6.016,4.432,10.984,10.206,11.852V18.18h-2.969v-3.154h2.969v-2.099c0-3.475,1.693-5,4.581-5 c1.383,0,2.115,0.103,2.461,0.149v2.753h-1.97c-1.226,0-1.654,1.163-1.654,2.473v1.724h3.593L19.73,18.18h-3.106v8.697 C22.481,26.083,27,21.075,27,15C27,8.373,21.627,3,15,3z" />
                            </svg>
                        </a>
                        <a href="">
                            <svg className="p-1 rounded-full border border-blue-500 fill-blue-500 transition-all duration-500 ease-in-out
                            hover:bg-blue-500 hover:fill-white" viewBox="0 0 32 32" width="36px" height="36px">
                                <path d="M 11.46875 5 C 7.917969 5 5 7.914063 5 11.46875 L 5 20.53125 C 5 24.082031 7.914063 27 11.46875 27 L 20.53125 27 C 24.082031 27 27 24.085938 27 20.53125 L 27 11.46875 C 27 7.917969 24.085938 5 20.53125 5 Z M 11.46875 7 L 20.53125 7 C 23.003906 7 25 8.996094 25 11.46875 L 25 20.53125 C 25 23.003906 23.003906 25 20.53125 25 L 11.46875 25 C 8.996094 25 7 23.003906 7 20.53125 L 7 11.46875 C 7 8.996094 8.996094 7 11.46875 7 Z M 21.90625 9.1875 C 21.402344 9.1875 21 9.589844 21 10.09375 C 21 10.597656 21.402344 11 21.90625 11 C 22.410156 11 22.8125 10.597656 22.8125 10.09375 C 22.8125 9.589844 22.410156 9.1875 21.90625 9.1875 Z M 16 10 C 12.699219 10 10 12.699219 10 16 C 10 19.300781 12.699219 22 16 22 C 19.300781 22 22 19.300781 22 16 C 22 12.699219 19.300781 10 16 10 Z M 16 12 C 18.222656 12 20 13.777344 20 16 C 20 18.222656 18.222656 20 16 20 C 13.777344 20 12 18.222656 12 16 C 12 13.777344 13.777344 12 16 12 Z" />
                            </svg>
                        </a>
                        <a href="">
                            <svg className="p-1 rounded-full border border-blue-500 fill-blue-500 transition-all duration-500 ease-in-out
                            hover:bg-blue-500 hover:fill-white" viewBox="0 0 30 30" width="36px" height="36px">
                                <path d="M24,4H6C4.895,4,4,4.895,4,6v18c0,1.105,0.895,2,2,2h18c1.105,0,2-0.895,2-2V6C26,4.895,25.105,4,24,4z M10.954,22h-2.95 v-9.492h2.95V22z M9.449,11.151c-0.951,0-1.72-0.771-1.72-1.72c0-0.949,0.77-1.719,1.72-1.719c0.948,0,1.719,0.771,1.719,1.719 C11.168,10.38,10.397,11.151,9.449,11.151z M22.004,22h-2.948v-4.616c0-1.101-0.02-2.517-1.533-2.517 c-1.535,0-1.771,1.199-1.771,2.437V22h-2.948v-9.492h2.83v1.297h0.04c0.394-0.746,1.356-1.533,2.791-1.533 c2.987,0,3.539,1.966,3.539,4.522V22z" />
                            </svg>
                        </a>
                    </div>
                </div>
                <div className='flex flex-col gap-4'>
                    <p className='flex items-start gap-2'>
                        <img src={location} className='w-8 h-8' alt="" />
                        <p className='flex flex-col gap-2'>
                            A108 Adam Street
                            <p>New York, NY 535022</p>
                        </p>
                    </p>
                    <p className='flex gap-2'>
                        <img src={message} className='w-8 h-8' alt="" />
                        <p>info@example.com</p>
                    </p>
                    <p className='flex gap-2'>
                        <img src={call} className='w-8 h-8' alt="" />
                        <p>+1 5589 55488 55s</p>
                    </p>
                </div>
                <div>
                    <form onSubmit={''} className="flex flex-col gap-5 min-[1100px]:ml-[-100px] lg:w-[450px] max-[1100px]:w-[100%] ">
                        <input className="border border-slate-500 p-2
                                        placeholder:text-black placeholder:text-sm" type="text" placeholder="Your Name" required />
                        <input className="border border-slate-500 p-2
                                        placeholder:text-black placeholder:text-sm " type="email" placeholder="Your Email" required />
                        <input className="border border-slate-500 p-2
                                        placeholder:text-black placeholder:text-sm " type="text" placeholder="Subject" required />
                        <textarea className="border border-slate-500 p-2
                                        placeholder:text-black placeholder:text-sm " name="" id="" cols="20" rows="5" placeholder="Your Message"></textarea>
                        <div className="mx-auto">
                            <button className="text-white font-bold py-2 px-8 rounded-3xl border border-[#3498db] bg-[#3498db] transition-all duration-300 ease-in-out
                                                hover:bg-[#30688d] ">Send Messages</button>
                        </div>
                    </form>
                </div>
            </div>
        </motion.section>
    )
}