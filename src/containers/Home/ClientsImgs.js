import img1 from '../../imgs/clients/client-1.png'
import img2 from '../../imgs/clients/client-2.png'
import img3 from '../../imgs/clients/client-3.png'
import img4 from '../../imgs/clients/client-4.png'
import img5 from '../../imgs/clients/client-5.png'
import img6 from '../../imgs/clients/client-6.png'

export const imgs = [
    {
        id: 1,
        img: img1
    },
    {
        id: 2,
        img: img2
    },
    {
        id: 3,
        img: img3
    },
    {
        id: 4,
        img: img4
    },
    {
        id: 5,
        img: img5
    },
    {
        id: 6,
        img: img6
    },
]