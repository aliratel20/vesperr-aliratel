import { motion, useAnimation } from "framer-motion";
import { useInView } from "react-intersection-observer";
// import { imgs } from "./ClientsImgs";
import { useEffect } from "react";
import hero from '../../imgs/hero-img.png'
import axios from "axios";
import { useState } from "react";

// Home Variants
const HomeAnime = (e) => ({
    visibleText: { opacity: 1, scale: 1, y: 0, transition: { duration: e } },
    hiddenText: { opacity: 0, scale: 0, y: 200 },
    visibleImg: { opacity: 1, scale: 1, x: 0, transition: { duration: 2 } },
    hiddenImg: { opacity: 1, scale: 1, x: 300 },
    visibleClients: { opacity: 1, scale: 1, transition: { duration: e } },
    hiddenClients: { opacity: 0, scale: 0 },
});


export default function Home() {
    //Control variables for animation
    const ControlHome = useAnimation()
    const ControlHomeImg = useAnimation()
    const ControlClientsImg = useAnimation()

    // Ref to pass control as a value
    const [ref, inView] = useInView()
    // Call the Animation 
    const [clients, setClients] = useState([])
    const [home, setHome] = useState({})

    //urls
    const apiUrl1 = '/Api.php?action=Client';
    const apiUrl2 = '/Api.php?action=Slider';
    const ImgUrl = 'http://'

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response1 = await axios.get(apiUrl1);
                const response2 = await axios.get(apiUrl2);

                // Update image URLs to include the base URL
                const FixFullImageUrls = response1.data.Client.map((client) => ({
                    ...client,
                    Image: ImgUrl + client.Image,
                }));
                setClients(FixFullImageUrls);
                setHome(response2.data.Slider[0])
                ControlHome.start("visibleText");
                ControlHomeImg.start("visibleImg");
                if (inView) {
                    ControlClientsImg.start("visibleClients");
                }
            } catch (error) {
                console.error('Axios request error:', error);
            }
        };

        fetchData();
    }, [inView]);

    return (
        <section>
            <div>
                <div className='flex justify-around items-center mt-32 my-10 mx-20 
                                        max-[980px]:flex-wrap-reverse max-md:mx-10 max-sm:mx-5'>
                    <div className='flex-1'>
                        <motion.h1
                            ref={ref}
                            variants={HomeAnime(0.5)}
                            initial="hiddenText"
                            animate={ControlHome}
                            className='text-5xl font-bold my-4 max-[500px]:text-3xl'>{home.title}</motion.h1>
                        <motion.p
                            ref={ref}
                            variants={HomeAnime(0.8)}
                            initial="hiddenText"
                            animate={ControlHome}
                            className='text-2xl text-gray-500 my-4 max-[500px]:text-lg'>{home.Discription}</motion.p>
                        <motion.button
                            ref={ref}
                            variants={HomeAnime(1.1)}
                            initial="hiddenText"
                            animate={ControlHome}
                            className='my-10 py-2 px-8 text-lg text-[#3498db] border-2 border-[#3498db] transition-all duration-500 ease-in-out rounded-3xl
                                            hover:bg-[#3498db] hover:text-white
                                            max-[500px]:text-sm '>
                            Get Started
                        </motion.button>
                    </div>
                    <div className='flex-2'>
                        <motion.div
                            ref={ref}
                            variants={HomeAnime()}
                            initial="hiddenImg"
                            animate={ControlHomeImg}
                        >
                            <img className='animate-[wiggle_3s_ease-in-out_infinite]' src={hero} width={'92%'} alt="" />
                        </motion.div>
                    </div>
                </div>
                {/* clients imgs */}
                <div className='flex flex-wrap justify-around items-center gap-5 px-20 py-5 w-full bg-blue-50 
                                        max-md:px-10'>
                    {
                        clients.map((e, index) => (
                            <motion.div key={index}
                                ref={ref}
                                variants={HomeAnime(index * 0.5)}
                                initial="hiddenClients"
                                animate={ControlClientsImg}
                            >
                                <img src={e.Image} className='grayscale hover:grayscale-0' width={'100px'} alt="" />
                            </motion.div>
                        ))
                    }
                </div>

            </div>

        </section>
    )
}
