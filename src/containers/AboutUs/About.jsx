import { motion, useAnimation } from "framer-motion";
import { useInView } from "react-intersection-observer";
import { useEffect } from "react";

import icon from '../../imgs/done_all_FILL0_wght400_GRAD0_opsz24.svg'
import count from '../../imgs/counts-img.svg'
import axios from "axios";
import { useState } from "react";

// About Variants
const AboutAnime = (e) => ({
    visibleText: { opacity: 1, scale: 1, y: 0, transition: { duration: e } },
    hiddenText: { opacity: 0, scale: 0, y: 10 },
    visibleImg: { opacity: 1, scale: 1, x: 0, transition: { duration: e } },
    hiddenImg: { opacity: 1, scale: 1, x: -100 },
    visibleItems: { opacity: 1, scale: 1, x: 0, transition: { duration: e } },
    hiddenItems: { opacity: 1, scale: 1, x: 100 },
});



export default function About() {
    const ControlText = useAnimation()
    const ControlImg = useAnimation()
    const ControlItems = useAnimation()

    // Ref to pass control as a value
    const [ref1, inView1] = useInView()
    const [ref2, inView2] = useInView()

    const [features, setFeatures] = useState([])
    const [loading, setIsLoading] = useState(0)
    //urls
    const apiUrl = '/Api.php?action=Aboutus';
    const ImgUrl = 'http://'
    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await axios.get(apiUrl);

                // Fix image URLs to include the base URL
                const FixFullImageUrls = response.data.About.Feature.map((Services) => ({
                    ...Services,
                    Icone: ImgUrl + Services.Icone,
                }));
                setFeatures(FixFullImageUrls);
                setIsLoading(false); // Set loading state to false when data is available
                // Call the Animation 
                if (inView1) {
                    ControlText.start("visibleText")
                }
                if (inView2) {
                    ControlImg.start("visibleImg")
                    ControlItems.start("visibleItems")
                }
            } catch (error) {
                console.error('Axios request error:', error);
            }
        };
        fetchData();
    }, [inView1], [inView2]);

    // Call the Animation 
    return (
        <section>
            {
                loading ?
                    <div>loading ...</div>
                    :
                    <div>
                        <div className="section-title text-center my-10">
                            <motion.h2
                                ref={ref1}
                                variants={AboutAnime(1)}
                                initial="hiddenText"
                                animate={ControlText}
                                className='text-3xl uppercase font-bold max-[500px]:text-xl'>ABOUT US</motion.h2>
                        </div>
                        <motion.div
                            ref={ref1}
                            variants={AboutAnime(1)}
                            initial="hiddenText"
                            animate={ControlText}
                            className='grid grid-cols-2 place-content-center mx-20
                           max-md:grid-cols-1 max-md:mx-10 max-sm:mx-2'>
                            <div className='my-5 p-[20px]'>
                                <p className='text-md mb-5'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                <div className='flex flex-col gap-2'>
                                    <p className='flex gap-2'>
                                        <img src={icon} alt="" />
                                        <span>Ullamco laboris nisi ut aliquip ex ea commodo consequat</span>
                                    </p>
                                    <p className='flex gap-2'>
                                        <img src={icon} alt="" />
                                        <span>Duis aute irure dolor in reprehenderit in voluptate velit</span>
                                    </p>
                                    <p className='flex gap-2'>
                                        <img src={icon} alt="" />
                                        <span>Ullamco laboris nisi ut aliquip ex ea commodo consequat</span>
                                    </p>
                                </div>
                            </div>
                            <div className='p-[20px] my-4'>
                                <p className='text-md mb-4'>
                                    Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
                                    reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                    Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </p>
                                <button className='text-sm text-[#3498db] font-bold border-2 border-[#3498db] py-2 px-6 rounded-3xl transition-all duration-500 ease-in-out
                                       hover:text-white hover:bg-[#3498db]'>
                                    Learn More
                                </button>
                            </div>
                        </motion.div>
                        <div className='flex justify-around items-center gap-8 mb-20 px-20 overflow-hidden
                            max-lg:flex-wrap max-md:px-10 max-[400px]:px-5'>
                            <motion.div
                                ref={ref2}
                                variants={AboutAnime(1.5)}
                                initial="hiddenImg"
                                animate={ControlImg}>
                                <img src={count} alt="" />
                            </motion.div>
                            <motion.div
                                ref={ref2}
                                variants={AboutAnime(1.5)}
                                initial="hiddenItems"
                                animate={ControlItems}
                                className='grid grid-cols-2 gap-2 place-content-center
                               max-md:grid-cols-1'>
                                {
                                    features.map((items) => (
                                        <div key={items.id}>
                                            <span className='flex gap-2 text-4xl font-bold my-4 '>
                                                <img src={items.Icone} width='50px' alt="" />
                                                {items.Title}
                                            </span>
                                            <p className='text-sm ml-14'>
                                                {items.Discription}
                                            </p>
                                        </div>
                                    ))
                                }
                            </motion.div>
                        </div>
                    </div>
            }
        </section>
    )
}