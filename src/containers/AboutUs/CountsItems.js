import smile from '../../imgs/sentiment_satisfied_FILL0_wght200_GRAD0_opsz24.svg'
import clock from '../../imgs/schedule_FILL0_wght200_GRAD0_opsz24.svg'
import pro from '../../imgs/diagnosis_FILL0_wght200_GRAD0_opsz24.svg'
import award from '../../imgs/social_leaderboard_FILL0_wght200_GRAD0_opsz24.svg'

export const Counts = [
    {
        id: 1,
        img: smile,
        number: 12,
        title: 'Happy Clients',
        p: ' consequuntur voluptas nostrum aliquid ipsam architecto ut.'
    },
    {
        id: 2,
        img: pro,
        number: 85,
        title: 'Projects',
        p: ' adipisci atque cum quia aspernatur totam laudantium et quia dere tan.'
    },
    {
        id: 3,
        img: clock,
        number: 18,
        title: 'Years of experience ',
        p: 'aut commodi quaerat modi aliquam nam ducimus aut voluptate non vel.'
    },
    {
        id: 4,
        img: award,
        number: 15,
        title: 'Awards',
        p: 'rerum asperiores dolor alias quo reprehenderit eum et nemo pad der.'
    },
]