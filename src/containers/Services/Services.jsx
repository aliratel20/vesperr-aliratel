import { motion, useAnimation } from "framer-motion";
import { useInView } from "react-intersection-observer";
import { useEffect, useState } from "react";
import OtherServices from './OtherServices';
import axios from "axios";


const SerAnime = (e) => ({
    visible: { opacity: 1, scale: 1, y: 0, transition: { duration: e } },
    hidden: { opacity: 0, scale: 1, y: 100 },
});

export default function Services() {

    const Control = useAnimation()
    const [isLoading, setIsLoading] = useState(true);


    // Ref to pass control as a value
    const [ref1, inView1] = useInView()
    const [items, setItems] = useState([])


    //urls
    const apiUrl = '/Api.php?action=Services';
    const ImgUrl = 'http://'

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await axios.get(apiUrl);

                // Fix image URLs to include the base URL
                const FixFullImageUrls = response.data.Services.Services.map((Services) => ({
                    ...Services,
                    Icone: ImgUrl + Services.Icone,
                }));
                setItems(FixFullImageUrls);
                setIsLoading(false); // Set loading state to false when data is available
                // Call the Animation 
                if (inView1) {
                    Control.start("visible");
                }
            } catch (error) {
                console.error('Axios request error:', error);
            }
        };
        fetchData();
    }, [inView1]);


    useEffect(() => {

    }, [inView1]);

    return (
        <section>
            {
                isLoading ? <div>loading ..</div>
                    :
                    <div>

                        <motion.div
                            ref={ref1}
                            variants={SerAnime(1)}
                            initial="hidden"
                            animate={Control}
                            className="section-title text-center my-10">
                            <h2 className="text-3xl uppercase font-bold my-2 max-[500px]:text-xl">Services</h2>
                            <p>Magnam dolores commodi suscipit eius consequatur ex aliquid fug</p>
                        </motion.div>
                        <div className='grid grid-cols-4 place-content-center gap-4 my-10
                            max-lg:grid-cols-2 max-sm:grid-cols-1 lg:mx-28 max-lg:mx-20 max-sm:mx-10'>
                            {
                                items.map((item, index) => (
                                    <motion.div
                                        variants={SerAnime(index * 0.2)}
                                        initial="hidden"
                                        animate={Control}
                                        className='services relative flex justify-items-start flex-col gap-4 p-[30px] bg-white shadow-xl rounded-md transition-all duration-500 ease-in-out z-10 overflow-hidden
                                        before:content-[""] before:bg-[#e1f0fa] before:absolute before:right-[-40px] before:top-[-40px] before:w-[100px] before:h-[100px] before:rounded-[50%] before:transition-all before:duration-300 before:z-[-10]
                                        before:hover:bg-[#3498db] before:hover:right-0 before:hover:top-0 before:hover:w-[100%] before:hover:h-[100%] before:hover:rounded-md hover:text-white'
                                        key={index} >
                                        <img src={item.Icone} alt="" />
                                        <a href='' className='text-lg font-bold'>{item.Title}</a>
                                        <p>{item.Discription}</p>
                                    </motion.div>
                                ))
                            }
                        </div>
                        <OtherServices />
                    </div>
            }
        </section>
    )
}
