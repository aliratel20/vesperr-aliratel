import img1 from '../../imgs/services/more/more-services-1.jpg'
import img2 from '../../imgs/services/more/more-services-2.jpg'
import img3 from '../../imgs/services/more/more-services-3.jpg'
import img4 from '../../imgs/services/more/more-services-4.jpg'

export const Items = [
    {
        id: 1,
        img: img1,
        title: 'Lobira Duno',
        p: 'Lorem ipsum dolor sit amet, consectetur elit, sed do eiusmod tempor ut labore et dolore magna aliqua.'
    },
    {
        id: 2,
        img: img2,
        title: 'Lobira Duno',
        p: 'Lorem ipsum dolor sit amet, consectetur elit, sed do eiusmod tempor ut labore et dolore magna aliqua.'
    },
    {
        id: 3,
        img: img3,
        title: 'Lobira Duno',
        p: 'Lorem ipsum dolor sit amet, consectetur elit, sed do eiusmod tempor ut labore et dolore magna aliqua.'
    },
    {
        id: 4,
        img: img4,
        title: 'Lobira Duno',
        p: 'Lorem ipsum dolor sit amet, consectetur elit, sed do eiusmod tempor ut labore et dolore magna aliqua.'
    },
]
