import { motion, useAnimation } from "framer-motion";
import { useInView } from "react-intersection-observer";
import { useEffect } from "react";
import { Items } from "./IitemsServices";
const SerAnime = (e) => ({
    visible: { opacity: 1, scale: 1, y: 0, transition: { duration: e } },
    hidden: { opacity: 0, scale: 1, y: 200 },
});

export default function OtherServices() {

    const ControlOther = useAnimation()
    const [ref1, inView1] = useInView()

    useEffect(() => {
        if (inView1)
            ControlOther.start("visible")
    }, [inView1])

    return (
        <div ref={ref1}
            className="grid grid-cols-2 gap-5 text-sm place-content-center mx-28 my-20
                        max-md:grid-cols-1 max-lg:mx-5 max-md:mx-24 max-[500px]:mx-2">
            {
                Items.map((e, index) => (
                    <motion.div
                        variants={SerAnime(index * 1)}
                        initial="hidden"
                        animate={ControlOther}
                        key={index} className='box relative'>
                        <div className='inbox absolute bottom-0 right-0 m-5 p-4 space-y-4 bg-[#ffffffef] rounded-lg z-10
                                        max-md:m-2 max-sm:p-1'>
                            <h1 className='text-center text-xl font-bold cursor-pointer'>{e.title}</h1>
                            <p className='text-gray-500'>{e.p}</p>
                            <button className='flex items-center gap-1 text-xs text-gray-500 uppercase hover:underline'>
                                <svg xmlns="http://www.w3.org/2000/svg" fill="gray" height="24" viewBox="0 -960 960 960" width="24">
                                    <path d="m553.846-253.847-42.153-43.384 152.77-152.77H180.001v-59.998h484.462l-152.77-152.77 42.153-43.384L779.999-480 553.846-253.847Z" />
                                </svg>
                                Read More
                            </button>
                        </div>
                        <img src={e.img} className='relative rounded-lg' alt="" />
                    </motion.div>
                ))
            }
        </div>
    )
}