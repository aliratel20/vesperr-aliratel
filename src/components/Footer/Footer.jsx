export default function Footer() {
    return (
        <div className="flex justify-between items-center gap-4 text-sm text-center px-20 py-10 border-t 
                        max-lg:flex-wrap max-lg:justify-center max-md:px-10">
            <p>
                © Copyright <b>Vesperr.</b> All Rights Reserved
                <p>
                    Designed by <b className="text-[#3498db]">BootstrapMade</b>
                </p>
            </p>
            <ul className="flex gap-4 max-sm:text-xs">
                <li className="hover:text-[#3498db]"><a href="">Home</a></li>
                <li className="hover:text-[#3498db]"><a href="">About</a></li>
                <li className="hover:text-[#3498db]"><a href="">Privacy</a></li>
                <li className="hover:text-[#3498db]"><a href="">Policy</a></li>
                <li className="hover:text-[#3498db]"><a href="">Terms of Use</a></li>
            </ul>
        </div>
    )
}