export const NavItems = [
    { id: 1, type: 'a', name: 'Home', herf: 'home', list: '' },
    { id: 2, type: 'a', name: 'About', herf: 'about', list: '' },
    { id: 3, type: 'a', name: 'Services', herf: 'services', list: '' },
    { id: 4, type: 'a', name: 'Portfolio', herf: 'portfolio', list: '' },
    { id: 5, type: 'a', name: 'Team', herf: 'team', list: '' },
    { id: 6, type: 'a', name: 'Pricing', herf: 'pricing', list: '' },
    {
        id: 7, type: 'drop', name: 'Drop Down', herf: '',
        list: [
            {
                id: 1, label: 'Drop Down 1', type: 'dd', list: ''
            },
            {
                id: 2, label: 'Deep Drop Down', type: 'ddd',
                list: [
                    {
                        id: 1, label: 'Deep Drop Down 1'
                    },
                    {
                        id: 2, label: 'Deep Drop Down 2'
                    },
                    {
                        id: 3, label: 'Deep Drop Down 3'
                    },
                    {
                        id: 4, label: 'Deep Drop Down 4'
                    },
                    {
                        id: 5, label: 'Deep Drop Down 5'
                    },
                ]
            },
            {
                id: 3, label: 'Drop Down 2', type: 'dd', list: ''
            },
            {
                id: 4, label: 'Drop Down 3', type: 'dd', list: ''
            },
            {
                id: 4, label: 'Drop Down 4', type: 'dd', list: ''
            },
        ]
    },
    { id: 8, type: 'a', name: 'Contact', herf: 'contact', list: '' },
    { id: 9, type: 'button', name: 'Get Started', herf: 'about', list: '' },
]