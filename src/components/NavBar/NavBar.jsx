import { useState, useEffect } from "react"
import { NavItems } from "./NavBarItems"
import menu from '../../imgs/NavBar/menu_FILL0_wght300_GRAD0_opsz24.svg'
import close from '../../imgs/NavBar/close_FILL0_wght300_GRAD0_opsz24.svg'
import axios from "axios"

export default function NavBar() {

    const [Scroll, setScroll] = useState(0)
    const [drop1, setDrop1] = useState(0)
    const [drop2, setDrop2] = useState(0)
    const [open, setOpen] = useState(0)

    const handle = (e) => {
        e.preventDefault();
        axios.get('http://reactapi.matrix-dev.sy/Api.php?action=Services')
            .then((response) => {
                // Handle the response
                console.log(response)
            })
            .catch((error) => {
                console.error('Axios request error:', error);
            });
    }
    useEffect(() => {
        const handleScroll = () => {
            if (window.scrollY > 135) {
                setScroll(true);
            } else {
                setScroll(false);
            }
        };
        window.addEventListener('scroll', handleScroll);
        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, [])

    return (
        <nav className={`fixed top-0 left-0 w-full bg-white flex justify-around items-center transition-all duration-300 ease-in-out z-20 
                        ${Scroll ? 'shadow-xl py-4' : 'shadow-none py-8'}
                        `}>
            <ul>
                <li>
                    <span className="text-3xl font-bold">Vesperr</span>
                </li>
            </ul>
            <div className={`${open ? 'max-[1000px]:absolute min-[1001px]:hidden top-0 w-[100%] h-[2000px] z-40 p-10 bg-[#000000c3]' : 'hidden'}`}>
                <button className="absolute top-0 right-0 mr-[6%] mt-5 z-60" onClick={() => setOpen(!open)}>
                    <img src={close} alt="" />
                </button>
            </div>
            <ul className={`flex gap-8 mt-1 
                           ${open ? 'max-[1000px]:fixed z-50 max-[1000px]:flex-col max-[1000px]:text-xl max-[1000px]:overflow-y max-[1000px]:bg-white max-[1000px]:top-0 max-[1000px]:mt-16 max-[1000px]:w-[90%] max-[1000px]:h-[70%] overflow-auto max-[1000px]:p-5 max-[1000px]:rounded-lg'
                    : 'max-[1000px]:hidden'}`}>
                {
                    NavItems.map((items, index) => {
                        {
                            switch (items.type) {
                                case 'a':
                                    return <li key={index} className="hover:text-[#3498db] py-2">
                                        <a href={items.herf}> {items.name}</a>
                                    </li>
                                case 'drop':
                                    return (
                                        <li onMouseOver={() => setDrop1(1)} onMouseOut={() => setDrop1(0)}
                                            key={index} className="relative flex cursor-pointer py-2  max-[1000px]:block">
                                            <a className="hover:text-[#3498db] flex">
                                                {items.name}
                                                <span className="material-symbols-outlined ">
                                                    expand_more
                                                </span>
                                            </a>
                                            <ul className={`${drop1 ? 'fixed flex flex-col py-2 z-50 mt-[25px] bg-white shadow-2xl max-[1000px]:relative max-[1000px]:mt-[10px]' :
                                                'hidden'} `}>
                                                {
                                                    items.list.map((e) => (
                                                        e.type === 'ddd' ?
                                                            <li onMouseOver={() => setDrop2(1)} onMouseOut={() => setDrop2(0)} key={e.id} className={`relative  flex px-4 py-2 max-[1000px]:block`}>
                                                                <a className={`hover:text-[#3498db] flex gap-4`}>{e.label} <span className="material-symbols-outlined">chevron_right</span></a>
                                                                <ul className={`${drop2 ? 'fixed z-50 flex flex-col gap-4 p-5 ml-[-195px] mt-[-10px] bg-white shadow-xl max-[1000px]:ml-0 max-[1000px]:relative max-[1000px]:mt-[10px]' : 'hidden'}`}>
                                                                    {
                                                                        e.list.map((d) => (
                                                                            <li key={d.id}><a className="hover:text-[#3498db]">{d.label}</a></li>
                                                                        ))
                                                                    }
                                                                </ul>
                                                            </li>
                                                            :
                                                            <li key={e.id} className={`flex px-4 py-2 ${drop1 ? 'relative' : 'hidden'}`}><a className={`hover:text-[#3498db]`}>{e.label}</a></li>
                                                    ))
                                                }
                                            </ul>
                                        </li>
                                    )
                                case 'button':
                                    return <li key={index} className="py-2">
                                        <a onClick={handle} href={items.herf} className="text-white bg-[#3498db] hover:bg-[#3498dbd0] py-2 px-6 rounded-3xl" > {items.name} </a>
                                    </li>
                            }
                        }

                    })
                }
            </ul>
            <ul className="min-[1000px]:hidden pt-2">
                <button onClick={() => { setOpen(!open) }}>
                    <img className={`${open ? 'hidden' : 'relative'}`} src={menu} alt="" />
                </button>
            </ul>
        </nav>
    )
}