import Footer from "./components/Footer/Footer";
import NavBar from "./components/NavBar/NavBar";
import About from "./containers/AboutUs/About";
import ContactUS from "./containers/ContactUS";
import Home from "./containers/Home/Home";
import Services from "./containers/Services/Services";

export default function App() {

  return (
    <div >
      <NavBar />
      <Home />
      <About />
      <Services />
      <ContactUS />
      <Footer />
    </div>
  )
}